import * as Swiper from '../../node_modules/swiper/dist/js/swiper.js';
import 'swiper';
import 'barba.js';
import * as Barba from '../../node_modules/barba.js/dist/barba.js';

// import './entry.es6';
import {ready2} from './entry';
// import {ContactMessage} from './BlugApi.dtos';
// import {listenToImages} from './entry';
import * as axios from '../../node_modules/axios/dist/axios.js';

let baseUrl  = document.location.hostname.lastIndexOf('.') > -1 ? "https://services.platymoose.ninja/" : "http://localhost:41232/";

console.log('es entry');
let tryContact = (f) => {

    try {

        let contactEmail = f.contactEmail;
        let contactName = f.contactName;
        let contactMsg = f.contactMsg;

        let message = {
            email: contactEmail.value,
            name: contactName.value,
            message: contactMsg.value
        };

        contactEmail.style.display = 'none';
        contactName.style.display = 'none';
        contactMsg.style.display = 'none';

        document.querySelectorAll('#contactForm button')[0].innerHTML = "Sending &hellip;";

        axios.post(`${baseUrl}/json/reply/ContactMessage` , message).then(() => {
            document.querySelectorAll('.formTitle')[0].textContent = "Thanks. I'll be in touch!";
            document.querySelectorAll('#contactForm button')[0].textContent = "Message received!";
            (document.querySelectorAll('#contactForm legend')[0]).style.display = 'none';
            (document.querySelectorAll('#submit')[0]).style.display = 'none';
            (document.querySelectorAll('#goBack')[0]).style.display = 'inline';

        }).catch(reason => {
            f.contactEmail.disabled = true;
            f.contactName.disabled = true;
            f.contactMsg.disabled = true;
            document.querySelectorAll('.formTitle')[0].textContent = "Something went wrong ...";
            document.querySelectorAll('#contactForm legend')[0].textContent = `Whoops! Maybe try again later.`;
            (document.querySelectorAll('#contactForm legend')[0]).title = `${reason}`;
            // (document.querySelectorAll('#submit')[0] as HTMLButtonElement).disabled = true;
            (document.querySelectorAll('#submit')[0]).style.display = 'none';
            (document.querySelectorAll('#goBack')[0]).style.display = 'inline';


        });

    } catch (e) {
        console.error(e);
    }
    return false;
};

function viewChanged() {
    try {
        window.scrollTo(0, 0);
        let t = document.querySelectorAll('img[data-action="zoom"]');
        for (let n = 0; n < t.length; n++)
            window['z'].a.setup(t[n]);
    } catch (e) {
    }
    // .card.
    // imagesLoaded('.coverImg')
    try {
        window["listenImages"]();
        window.makeGallery();
    } catch (e) {
    }
}

function initPjax() {
    console.log('ts load');
    Barba.Pjax.Dom.wrapperId = 'pj-wrap';
    Barba.Pjax.Dom.containerClass = 'pj-container';

    Barba.Pjax.start();
    Barba.Prefetch.init();
    Barba.transitionLength = 1000;
    Barba.Dispatcher.on('transitionCompleted', viewChanged);
    // window.BTX = BTX;
    Barba.Dispatcher.on('linkClicked', () => document.getElementById('menu-switch')['checked'] = false);
    window["Swiper"] = Swiper;
    window["Barba"] = Barba;
}

if (document.location.host.indexOf('.') > 0 && document.location.protocol == 'http:') {
    document.location.protocol = 'https:';
}

console.log('ts main');
document.addEventListener("DOMContentLoaded", initPjax);

// Object.assign(window, {tryContact: tryContact});
window['tryContact'] = tryContact;
window['Swiper'] = Swiper;
document.documentElement.className += ' loaded';
document.documentElement.className += ' hasscript';
document.documentElement.className = document.documentElement.className.replace('noscript', '');
// window.tryContact = tryContact;
ready2();