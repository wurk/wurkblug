import Clipboard from 'clipboard';
import './zoom.min';
import './sw-register';
import 'barba.js';

var imagesLoaded = require('imagesloaded');
function sizeDoc() {
    var sizes = [70, 80, 90, 100];

    var el = document.querySelectorAll("#centerBody>article")[0];
    if (el && el.classList) {
        for (var n = 0; n <= 3; n++) if (el.classList.contains('size_' + sizes[n])) el.classList.remove('size_' + sizes[n]);

        el.classList.add('size_' + window.localStorage.getItem('zoom'));
    }
}

function setDocSize(size) {
    window.localStorage.setItem('zoom', (size < 100 && size > 69 ? size : null) || 100);
    sizeDoc();
}

function closeMenu() {
    menuToggle().checked = false;
}

function openMenu() {
    menuToggle().checked = true;
}

function menuToggle() {
    return document.getElementById('menu-switch');
}

function loaded() {
    closeMenu();
    sizeDoc();
    try {
        var clipboard = new Clipboard('.permalink-wide');
        muut() && muut().destroy();
        $('a.muut').muut();
    } catch (ex) {}
}

function clicked(e) {
    if (menuToggle().checked && e.clientX > 260) {
        closeMenu();
    }
}

function loadBackground() {
    document.documentElement.setAttribute('class', 'loaded');
}

// export function initPjax() {
//
//     Barba.Pjax.wrapperId = 'centerBody';
//     Barba.Pjax.containerClass = 'pj-container';
//     Barba.Pjax.start();
//     Barba.Prefetch.init();
// }

export function ready2() {
    document.addEventListener("DOMContentLoaded", function () {
        console.log('es6 load');
        // document.addEventListener('turbolinks:load', loaded);
        // document.addEventListener('turbolinks:render', rendered);
        document.addEventListener('click', clicked);

        // window.setTimeout(loadBackground, 140);
        loaded();
        listenToImages();
    });
}

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
    };
}

function findAncestor(el, sel) {
    if (typeof el.closest === 'function') {
        return el.closest(sel) || null;
    }
    while (el) {
        if (el.matches(sel)) {
            return el;
        }
        el = el.parentElement;
    }
    return null;
}

export function listenToImages() {
    var imgLoad = imagesLoaded('#blocks');
    console.log('listening to image events');
    imgLoad.on('always', function (il) {
        console.log('loaded all images');
    });
    imgLoad.on('progress', function (instance, image) {
        console.log('image is ' + (image.isLoaded ? 'loaded' : 'broken') + ' for ' + image.img.src);
        var ancest = findAncestor(image.img, 'article.boxCard');
        try {
            ancest.className = ancest.className.replace('deferred', '');
            ancest.className += ' animated fadeIn slow imageLoaded ';
        } catch (x) {
            console.error(x);
        }
    });
}

if (typeof Object.assign != 'function') {
    Object.assign = function (target, varArgs) {
        // .length of function is 2
        'use strict';

        if (target == null) {
            // TypeError if undefined or null
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var to = Object(target);

        for (var index = 1; index < arguments.length; index++) {
            var nextSource = arguments[index];

            if (nextSource != null) {
                // Skip over if undefined or null
                for (var nextKey in nextSource) {
                    // Avoid bugs when hasOwnProperty is shadowed
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
        }
        return to;
    };
}

window.localStorage.setItem('zoom', window.localStorage.getItem('zoom') || 100);
window["listenImages"] = listenToImages;
window["sizeDoc"] = sizeDoc;
window["setDocSize"] = setDocSize;
window["ready2"] = ready2;
ready2();
//# sourceMappingURL=entry.js.map