function sizeDoc() {
    var sizes = [70, 80, 90, 100];

    var el = document.querySelectorAll("#centerBody>article")[0];
    if (el && el.classList) {
        for (var n = 0; n <= 3; n++)
            if (el.classList.contains("size_" + sizes[n])) el.classList.remove("size_" + sizes[n]);

        el.classList.add("size_" + window.localStorage.getItem("zoom"));
    }
}

function setupImageZoom() {
    var t = document.querySelectorAll('img[data-action="zoom"]');
    for (var n = 0; n < t.length; n++)
        window.z.a.setup(t[n]);
}

function setDocSize(size) {
    window.localStorage.setItem("zoom", (size < 100 && size > 69 ? size : null) || 100);
    sizeDoc();
}

function rendered() {}

function loaded() {
    try {
        closeMenu();
        sizeDoc();
        setupImageZoom();
        new Clipboard(".permalink-wide");
        muut() && muut().destroy();
        $("a.muut").muut();
    } catch (e) {}
}

function clicked(e) {
    if (menuToggle().checked && e.clientX > 260) {
        closeMenu();
    }
}

function closeMenu() {
    menuToggle().checked = false;
}

function openMenu() {
    menuToggle().checked = true;
}

function menuToggle() {
    return document.getElementById("menu-switch");
}

function loadBackground() {
    document.documentElement.setAttribute("class", "loaded");
}

function ready() {
    document.addEventListener("turbolinks:load", loaded);
    document.addEventListener("turbolinks:render", rendered);
    document.addEventListener("click", clicked);
    window.setTimeout(loadBackground, 500);
}


// zoomm();

window.localStorage.setItem("zoom", window.localStorage.getItem("zoom") || 100);
document.addEventListener("DOMContentLoaded", ready, false);

sizeDoc();
